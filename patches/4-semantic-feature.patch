diff --git a/pom.xml b/pom.xml
index b1bea5ba..dcb31cc1 100644
--- a/pom.xml
+++ b/pom.xml
@@ -18,7 +18,7 @@
     <frontend-maven-plugin.version>1.12.1</frontend-maven-plugin.version>
     <java.version>17</java.version>
     <maven-release-plugin.version>3.0.0-M7</maven-release-plugin.version>
-    <maven-checkstyle-plugin.version>3.2.0</maven-checkstyle-plugin.version>
+    <maven-checkstyle-plugin.version>3.2.1</maven-checkstyle-plugin.version>
     <node.lts.version>v16.18.1</node.lts.version>
   </properties>
   
diff --git a/src/main/java/tech/kinsaleins/rva/controller/GreetingController.java b/src/main/java/tech/kinsaleins/rva/controller/GreetingController.java
new file mode 100644
index 00000000..ae88552c
--- /dev/null
+++ b/src/main/java/tech/kinsaleins/rva/controller/GreetingController.java
@@ -0,0 +1,54 @@
+/*
+ * The MIT License
+ *
+ * Copyright 2023 timothystone.
+ *
+ * Permission is hereby granted, free of charge, to any person obtaining a copy
+ * of this software and associated documentation files (the "Software"), to deal
+ * in the Software without restriction, including without limitation the rights
+ * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
+ * copies of the Software, and to permit persons to whom the Software is
+ * furnished to do so, subject to the following conditions:
+ *
+ * The above copyright notice and this permission notice shall be included in
+ * all copies or substantial portions of the Software.
+ *
+ * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
+ * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
+ * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
+ * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
+ * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
+ * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
+ * THE SOFTWARE.
+ */
+
+package tech.kinsaleins.rva.controller;
+
+import java.util.concurrent.atomic.AtomicLong;
+import org.springframework.web.bind.annotation.GetMapping;
+import org.springframework.web.bind.annotation.RequestParam;
+import org.springframework.web.bind.annotation.RestController;
+import tech.kinsaleins.rva.model.Greeting;
+
+/**
+ * The Spring REST controller for Greeting.
+ *
+ * @author timothystone
+ */
+@RestController
+public class GreetingController {
+
+  private static final String TEMPLATE = "Hello, %s!";
+  private final AtomicLong counter = new AtomicLong();
+
+  /**
+   * The greeting endpoint.
+   * 
+   * @param name The name for the Greeting.
+   * @return a {@link Greeting} record.
+   */
+  @GetMapping("/greeting")
+  public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
+    return new Greeting(counter.incrementAndGet(), String.format(TEMPLATE, name));
+  }
+}
