# Richmond Virginia Software Developers User Group (RVASDUG)

> The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and
"OPTIONAL" in this document are to be interpreted as described in [RFC 2119](https://www.rfc-editor.org/rfc/rfc2119).

## CICD with GitLab

Let's define CICD.

### *Continuous Integration/Continuous (Deployment|Delivery)*

Continuous **Deployment** differs from Continuous **Delivery** in one simple way: an approval "gate."

In *Deployment* there is an approval before Production, *Delivery* is fully automated.

Full automation is achievable, but CICD SHOULD NOT be an afterthought. CICD MUST avoid Conway's Law.

**Conway's Law**

*Conway’s Law* is an IT theory created by computer scientist/programmer Melvin Conway in 1967.

> Organizations, who design systems, are constrained to produce designs which are copies of the communication 
> structures of these organizations.

The theory gained popularity when it was cited by Fred Brooks in the iconic book *The Mythical Man Month*. (If you're 
unfamiliar with Brooks or *TMM*, the *TMM* is simply the idea that throwing resources—people or money—at a delivery 
date does not shorten the time to delivery, i.e., nine (9) women cannot have a baby in one month. Brooks also coined 
the term, the "second-system effect," or "second-system syndrome,": the tendency of small, elegant, and successful 
systems to be succeeded by over-engineered, bloated systems, due to inflated expectations and overconfidence. 

Douglas Crockford, the *creator* of JSON, [has a great presentation on software quality](https://www.youtube.
com/watch?v=t9YLtDJZtPY) discussing both concepts. Pro-tip: watch at 1.5x if you don't think you have the time for 45 mins. You'll laugh. You'll cry. You'll laugh again. The presentation is almost 20 years old, so when he talks about software generations 
being 20 years old, consider today's news.

### Start With CICD

CICD becomes much harder after a project is in production. Manual testing, manual deploys, manual *fill-in-the-blank*. 
Complacency sets in and organization communication structures, Conway's Law, add friction to any effort.

CICD SHOULD be set out from the beginning.

## The Project

This project begins with a simple Spring Initializr Spring Web project.

The project contains tooling that may not be immediately obvious, but features:

* Husky (for githooks)
* Maven Checkstyle
* Conventional Commits by commitlint

These come into play later to help drive Trunk Based development.

As we progress through the project presentation, and noted in the *commits* for those following later, we perform the following:

1. [Add the GitLab Maven CI template](#add-the-gitlab-ci-template), [patch](patches/1-basic-template.patch)
2. [Upgrade to the To Be Continuous Maven template](#upgrade-to-the-to-be-continuous-maven-template), [patch](patches/2-tbc-maven-template.patch)
3. [Add Semantic Release](#add-semantic-release), [patch](patches/3-tbc-semantic-template.patch)
4. [Add a Feature](#add-a-feature), [patch](patches/4-semantic-feature.patch)
5. [Add a Test](#add-a-test), [patch](patches/5-semantic-test.patch)

Each step is discussed below.

> If you want to reproduce the presentation, open a GitLab account. Create a group and a Blank Project (no README!). 
> Clone or Fork this project to your group. Follow the instructions presented by GitLab to push the existing code if 
> you cloned the project. **Before you push, run `git reset --hard 0.0.1` *then* push the code. All the patches will 
> be available and you can reproduce the presentation.

# Add the GitLab CI template

`git apply patches/1-basic-template.patch`

GitLab comes with [a lot of templates](https://docs.gitlab.com/ee/ci/examples/index.html#cicd-templates). We start with 
the GitLab Maven Template. This template has branch and `master|main` triggers. Branch commits trigger the `verify` 
job. Merges to `master|main` trigger the `deploy` job. 

There is no versioning, except manual intervention. The template leverages the most basic elements and behaviors of 
the Maven lifecycle. It's pretty direct and serves as a starting point.

# Upgrade to the To Be Continuous Maven Template

`git apply patches/2-tbc-maven-template.patch`

Enterprise Pipelines Unlocked! To Be Continuous bills itself as: **GitLab CI, made easy... Build professional 
pipelines in minutes.** And you'll immediately see why.

With less than 20 lines of code, and most of it boilerplate and hidden, the pipeline immediately becomes robust. We 
have GitLab `stages` and `jobs` defined that align to common CICD practices, including Git Flow... (but why?!)

Branches are pushed and trigger Maven SNAPSHOT releases. As Merge Requests—Pull Requests by any other name—are 
created, Merge Request pipelines are initiated.

Coupled with group and project settings, trunk based development is ready to be exploited.

The Maven template uses the powerful Maven Release Plugin to automate all elements of a release to a repository, 
including tagging the release, and more. While it automates patch versioning, it can leave minor and major versioning 
manual, and with it, the friction of promotion.

# Add Semantic Release

`git apply patches/3-tbc-semantic-template.patch`

With TBC Maven added, the project can begin taken advantage of TBC templates that add even more functionality. It's 
just plug-and-play at this point.

Semantic Release is a project that aims to provide pluggable tools—and a "Kill All Humans" ethos—by promoting 
"unromantic and unsentimental" versioning. 

The Semantic Release template looks at commits since a tag—recall the Maven Release Plugin will tag a repository—and 
then use those commit messages to determine expectations of the development team. Huh? How?

Semantic Versioning dictates that *major* releases are breaking changes to a published API, *minor* releases are 
features, and *patch* versions are fixes. Anything else is ephemeral (my favorite word).

Recall the use of `husky` (githooks... made easy) and `commitlint` in the project? These tools provide a control for 
enabling Semantic Release and Semantic versioning.

This project uses [Another Caffeinated Day's Commit Controls project](https://gitlab.com/another15y/commit-controls/) 
for "coupling Checkstyle with pre-commit Git hooks". Making it a little easier in Javaland. 

# Add a Feature

`git apply patches/4-semantic-feature.patch`

And now we see the power of the Semantic Release and Maven templates of To Be Continuous, with `commitlint`—and the 
[conventional commits specification](https://www.conventionalcommits.org/en/v1.0.0/)—all lining up to release a 
"feature."

# Add a Test

`git apply patches/5-semantic-test.patch`

And a test is added that doesn't trigger a version, and while the `mvn-release` job is provided, executing it will run 
afoul of duplicate tags or blocked by your repository. One doesn't overwrite release artifacts. Either 
will fail the deployment. No-hassle controls by leveraging the built in best practices of Git and Maven.
