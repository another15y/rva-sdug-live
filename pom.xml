<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>3.0.2</version>
    <relativePath /> <!-- lookup parent from repository -->
  </parent>
  <groupId>tech.kinsaleins</groupId>
  <artifactId>rva-sdug</artifactId>
  <version>1.0.1-SNAPSHOT</version>
  <name>RVA SDUG Gitlab CICD+</name>
  <description>A GitLab CICD demo and a little bit more, with an introduction to To Be Continuous</description>
  <properties>
    <checkstyle.version>10.6.0</checkstyle.version>
    <frontend-maven-plugin.version>1.12.1</frontend-maven-plugin.version>
    <java.version>17</java.version>
    <maven-release-plugin.version>3.0.0-M7</maven-release-plugin.version>
    <maven-checkstyle-plugin.version>3.2.0</maven-checkstyle-plugin.version>
    <node.lts.version>v16.18.1</node.lts.version>
  </properties>
  
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.puppycrawl.tools</groupId>
        <artifactId>checkstyle</artifactId>
        <version>${checkstyle.version}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>
  
  <dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.puppycrawl.tools</groupId>
      <artifactId>checkstyle</artifactId>
    </dependency>
  </dependencies>
    
  <scm>
    <connection>scm:git:https://gitlab.com/another15y/rva-sdug-live.git</connection>
    <developerConnection>scm:git:https://gitlab.com/another15y/rva-sdug-live.git</developerConnection>
    <url>https://gitlab.com/another15y/rva-sdug-live</url>
    <tag>HEAD</tag>
  </scm>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven-release-plugin.version}</version>
          <configuration>
            <tagNameFormat>@{version}</tagNameFormat>
          </configuration>
        </plugin>
        <plugin>
          <groupId>com.github.eirslett</groupId>
          <artifactId>frontend-maven-plugin</artifactId>
          <version>${frontend-maven-plugin.version}</version>
          <configuration>
            <installDirectory>target</installDirectory>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-checkstyle-plugin</artifactId>
          <version>${maven-checkstyle-plugin.version}</version>
          <dependencies>
            <dependency>
              <groupId>com.puppycrawl.tools</groupId>
              <artifactId>checkstyle</artifactId>
              <version>${checkstyle.version}</version>
            </dependency>
          </dependencies>
          <configuration>
            <!-- the checkstyle plugin can be configured here       -->
          </configuration>
          <executions>
            <execution>
              <id>validate</id>
              <phase>validate</phase>
              <goals>
                <goal>check</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven-compiler-plugin.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>
    
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <artifactId>maven-release-plugin</artifactId>
      </plugin>
    </plugins>
  </build>
  
  <profiles>
    <profile>
      <id>husky</id>
      <build>
        <plugins>
          <plugin>
            <groupId>com.github.eirslett</groupId>
            <artifactId>frontend-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>install-node-and-npm</id>
                <goals>
                  <goal>install-node-and-npm</goal>
                </goals>
                <configuration>
                  <nodeVersion>${node.lts.version}</nodeVersion>
                </configuration>
              </execution>
              <execution>
                <id>npm install</id>
                <goals>
                  <goal>npm</goal>
                </goals>
              </execution>
              <execution>
                <id>husky install</id>
                <goals>
                  <goal>npm</goal>
                </goals>
                <phase>generate-resources</phase>
                <configuration>
                  <arguments>run husky:install</arguments>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
  
  <repositories>
    <repository>
      <id>gitlab-maven</id>
      <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </repository>
  </repositories>
  <distributionManagement>
    <repository>
      <id>gitlab-maven</id>
      <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </repository>
    <snapshotRepository>
      <id>gitlab-maven</id>
      <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </snapshotRepository>
  </distributionManagement>
  
  <developers>
    <developer>
      <id>timothystone</id>
      <name>Timothy Stone</name>
      <timezone>America/New_York</timezone>
      <email>gitlab@petmystone.com</email>
      <roles>
        <role>architect</role>
        <role>creator</role>
      </roles>
      <properties>
        <avatar>https://gravatar.com/avatar/6d113d4c7d893b6bb96e30e36e7905fe</avatar>
        <gpg.public-key>
          https://keys.openpgp.org/vks/v1/by-fingerprint/3E66175B263B9BB566F2062DC79372064388F770
        </gpg.public-key>
      </properties>
    </developer>
  </developers>
  
  <licenses>
    <license>
      <name>MIT</name>
      <url>https://opensource.org/licenses/MIT</url>
    </license>
  </licenses>

</project>
